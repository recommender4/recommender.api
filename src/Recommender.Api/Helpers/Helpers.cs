using System;
using System.IO;
using Microsoft.Extensions.Logging;
using SixLabors.ImageSharp;

namespace Recommender.Api.Core
{
    public class Helpers
    {
        private ILogger logger;
        public Helpers(ILogger<Helpers> logger)
        {
            this.logger = logger;
        }
        internal string GetRandomDate()
        {
            try
            {
                DateTime endDate = new DateTime(2021, 1, 1, 10, 0, 0);
                DateTime startDate = new DateTime(1980, 1, 1, 10, 0, 0);

                var randomTest = new Random();

                TimeSpan timeSpan = endDate - startDate;
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, (int)timeSpan.TotalMinutes), 0);
                DateTime newDate = startDate + newSpan;

                string randomDate = newDate.Date.ToString("yyyy-MM-dd");

                return randomDate;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                throw;
            }
        }

        // internal void CheckImageSize(string backdropUrl)
        // {
        //     try
        //     {
        //        using(Image image = Image.Load())
        //     }
        //     catch (Exception ex)
        //     {
        //         logger.LogError(ex.ToString());
        //         throw;
        //     }
        // }
    }
}