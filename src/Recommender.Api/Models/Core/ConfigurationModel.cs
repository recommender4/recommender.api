using System;

namespace Recommender.Api.Models.Core
{
    public class Configuration
    {
        public TMDBConfiguration TMDB
        {
            get
            {
                return new TMDBConfiguration();
            }
        }

        public CoreConfiguration Core
        {
            get
            {
                return new CoreConfiguration();
            }
        }


        public class TMDBConfiguration
        {
            private string _apiKey = Environment.GetEnvironmentVariable("TMDB_API_KEY");
            private string _apiBaseUrl = Environment.GetEnvironmentVariable("TMDB_API_BASE_URL");

            public string ApiKey
            {
                get
                {
                    if(!string.IsNullOrEmpty(_apiKey))
                    {
                        return _apiKey;
                    }
                    throw new ArgumentException($"Configuration error, missing environment variable: TMDB_API_KEY");
                }
            }
            public string ApiBaseUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_apiBaseUrl))
                    {
                        return _apiBaseUrl;
                    }
                    throw new ArgumentException($"Configuration error, missing environment variable: TMDB_API_BASE_URL");
                }
            }

        }

        public class CoreConfiguration
        {
            private int _matchWeight = Convert.ToInt16(Environment.GetEnvironmentVariable("MATCH_WEIGHT"));

            public int MatchWeight
            {
                get
                {
                    if(_matchWeight > 0)
                    {
                        return _matchWeight;
                    }
                    throw new ArgumentException($"Configuration error, missing environment variable: MATCH_WEIGHT");

                }
            }
        }
    }
}