using System.Collections.Generic;

namespace Recommender.Api.Models.Movie
{
    public class Details
    {
        public int Id { get; set; }
        public List<int> Genres { get; set; }
        public List<int> ProductionCompanies { get; set; }
    }
}