using System;
using System.Collections.Generic;
using FuzzySharp;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Recommender.Api.Core;
using Recommender.Api.Models.Core;
using Recommender.Api.Models.Movie;
using Recommender.Api.Models.TMDB.Movie;
using RestSharp;

namespace Recommender.Api.Factories
{
    public class TMDBFactory
    {
        private readonly ILogger logger;
        private readonly Configuration configuration;
        private readonly Helpers helpers;

        public TMDBFactory(ILogger<TMDBFactory> logger, Configuration configuration, Helpers helpers)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.helpers = helpers;
        }

        internal int GetMovieIdByTitle(string title)
        {
            try
            {
                var client = new RestClient($"{configuration.TMDB.ApiBaseUrl}/search/movie?api_key={configuration.TMDB.ApiKey}&language=en-US&query={title}&page=1&include_adult=false");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    MovieSearchResults movieResults = JsonConvert.DeserializeObject<MovieSearchResults>(response.Content);
                    if (movieResults != null)
                    {
                        foreach (var movie in movieResults.Results)
                        {
                            var ratio = Fuzz.WeightedRatio(movie.Title, title);
                            if (ratio >= configuration.Core.MatchWeight)
                            {
                                return movie.Id;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception(response.ErrorMessage);
                }
                return 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                throw;
            }
        }

        internal string GetRandomBackground()
        {
            try
            {
                string startDate = helpers.GetRandomDate();
                string endDate = helpers.GetRandomDate();
                var client = new RestClient($"{configuration.TMDB.ApiBaseUrl}/discover/movie?api_key={configuration.TMDB.ApiKey}&language=en-US&region=US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate&release_date.gte=2019-01-01&release_date.lte=2020-01-01&with_original_language=en");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if(response.IsSuccessful)
                {
                    DiscoverModel discoverResults = JsonConvert.DeserializeObject<DiscoverModel>(response.Content);
                    int resultCount = discoverResults.Results.Count;

                    Random rnd = new Random();
                    int rInt = rnd.Next(0, resultCount);

                    // string backdropUrl = "https://www.themoviedb.org/t/p/w1280" + discoverResults.Results[rInt].BackdropPath;
                    string backdropUrl = "https://www.themoviedb.org/t/p/original" + discoverResults.Results[rInt].BackdropPath;

                    // helpers.CheckImageSize(backdropUrl);

                    return backdropUrl;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                throw;
            }
        }

        internal Details GetMovieDetailsbyId(int id)
        {
            try
            {
                var client = new RestClient($"{configuration.TMDB.ApiBaseUrl}/movie/{id}?api_key={configuration.TMDB.ApiKey}&language=en-US");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    TMDBMovieDetails tmdbMovieDetails = JsonConvert.DeserializeObject<TMDBMovieDetails>(response.Content);
                    if (tmdbMovieDetails != null)
                    {
                        Details movieDetails = new Details();
                        movieDetails.Genres = new List<int>();
                        movieDetails.ProductionCompanies = new List<int>();

                        foreach (var genre in tmdbMovieDetails.Genres)
                        {
                            movieDetails.Genres.Add(genre.Id);
                        }

                        foreach (var productionCompany in tmdbMovieDetails.ProductionCompanies)
                        {
                            movieDetails.ProductionCompanies.Add(productionCompany.Id);
                        }
                        movieDetails.Id = id;
                        return movieDetails;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                throw;
            }
        }
    }
}