﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recommender.Api.Factories;
using Recommender.Api.Models.Movie;

namespace Recommender.Api.Controllers
{
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly ILogger logger;
        private readonly TMDBFactory tmdbFactory;
        public MovieController(ILogger<MovieController> logger, TMDBFactory tmdbFactory)
        {
            this.logger = logger;
            this.tmdbFactory = tmdbFactory;
        }

        // GET movie/the matrix
        [HttpGet("{title}")]
        public IActionResult Get(string title)
        {
            logger.LogInformation($"Request received to get inforamtion for {title}");
            int movieId = tmdbFactory.GetMovieIdByTitle(title);
            Details movieDetails = tmdbFactory.GetMovieDetailsbyId(movieId);

            if (movieId > 0)
            {
                return Ok(movieDetails);
            }
            
            return NotFound();
        }
    }
}
