﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recommender.Api.Factories;
using Recommender.Api.Models.Movie;

namespace Recommender.Api.Controllers
{
    [Route("[controller]")]
    public class WebController : ControllerBase
    {
        private readonly ILogger logger;
        private readonly TMDBFactory tmdbFactory;
        public WebController(ILogger<WebController> logger, TMDBFactory tmdbFactory)
        {
            this.logger = logger;
            this.tmdbFactory = tmdbFactory;
        }

        // GET web/background
        [HttpGet("background")]
        public IActionResult Get()
        {
            string backgroundUrl = tmdbFactory.GetRandomBackground();
            if(!string.IsNullOrEmpty(backgroundUrl))
            {
                return Ok(backgroundUrl);
            }
            return NotFound();

        }
    }
}
