﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Recommender.Api.Core;
using Recommender.Api.Factories;
using Recommender.Api.Models.Core;

namespace Recommender.Api
{
    public class Startup
    {
        readonly string s3Origin = "_s3Origin";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddTransient<TMDBFactory>();
            services.AddTransient<Configuration>();
            services.AddTransient<Helpers>();

            services.AddCors(options =>
            {
                options.AddPolicy(name: s3Origin,
                                builder =>
                                {
                                    builder.AllowAnyHeader()
                                    .AllowAnyMethod()
                                    .AllowAnyOrigin();
                                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(s3Origin);


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Welcome to running ASP.NET Core on AWS Lambda");
                });
            });
        }
    }
}
